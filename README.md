# Overview

This setup partially automates the installation of the Lab

## SETUP
1. Install pip (*nix commands)
```bash
    sudo easy_install pip
```

2. Install ansible
```bash
    pip install ansible
```

3. Install dependent modules
```bash
pip install -r requirement.txt
```
## INTRODUCTION

1. Add ssh-keys as such

```bash
    for ip in 13.229.45.235 52.221.252.163 54.255.170.199 52.77.212.12 13.228.25.101 54.169.101.71 13.229.112.114 ; do ssh-copy-id archSt
udent@$ip; done
```

2. Install splunk

```bash
ansible-playbook install-splunk.yml
```

3. Encrypt splunk secrets

```bash
echo <ansible vault password> >> ./.vault_pass.txt
mkdir ./secret_vars
echo "---" > ./secret_vars/common.yml
ansible-vault encrypt_string <Splunk Password|ex. ExpertInsight> --name admin_pwd --vault-id ./.vault_pass.txt >> ./secret_vars/common.yml
```

4. Check all the configuration items
fill the [host file](etc/ansible/hosts) with IPs against each of the host from the exam doco

5. Run the Ansible play

```bash
ansible-playbook install-splunk.yml
```