FROM python:2.7

RUN pip install ansible==2.5.2 

VOLUME [ "/ansible/playbook" ]

WORKDIR /ansible/playbook